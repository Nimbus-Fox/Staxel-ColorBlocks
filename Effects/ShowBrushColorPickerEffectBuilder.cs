﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Effects;
using Staxel.Logic;

namespace NimbusFox.ColorBlocks.Effects {
    class ShowBrushColorPickerEffectBuilder : IEffectBuilder, IDisposable {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.effect.showBrushColorPicker";
        }

        public IEffect Instance(Timestep step, Entity entity, EntityPainter painter, EntityUniverseFacade facade, Blob data,
            EffectDefinition definition, EffectMode mode) {
            return new ShowBrushColorPickerEffect(data, facade);
        }
    }
}
