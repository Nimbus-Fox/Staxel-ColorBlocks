﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.PlayerCommands;
using NimbusFox.KitsuneCore.V1.UI.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.ColorBlocks.Effects {
    class ShowBlockColorPickerEffect : IEffect, IDisposable {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public bool Completed() {
            return _completed;
        }

        private Color _color = Color.White;

        public ShowBlockColorPickerEffect(Blob data, EntityUniverseFacade facade) {
            if (!data.Contains("target")) {
                return;
            }

            var players = new Lyst<Entity>();

            facade.GetPlayers(players);

            foreach (var player in players) {
                if (ClientContext.PlayerFacade.IsLocalPlayer(player)) {
                    if (player.Id.Id == data.GetLong("target")) {
                        _show = true;
                        break;
                    }
                }
            }

            if (!_show) {
                return;
            }

            if (data.Contains("blockColor")) {
                if (uint.TryParse(data.GetLong("blockColor").ToString(), out var col)) {
                    _color = ColorMath.FromRgba(col);
                }
            }
        }

        private bool _completed;

        private bool _show;

        private static ColorPickerWindow _current;

        public void Render(Entity entity, EntityPainter painter, Timestep renderTimestep, DeviceContext graphics,
            ref Matrix4F matrix,
            Vector3D renderOrigin, Vector3D position, RenderMode renderMode) {
            if (!_show) {
                _completed = true;
                return;
            }
            if (!Completed()) {
                _completed = true;

                _current?.Dispose();

                _current = new ColorPickerWindow(_color);

                _current.OnColorSet += color => {
                    var cmd = BlobAllocator.Blob(true);
                    cmd.SetString("action", ChangeBlockColorCommand.KindCode());
                    cmd.SetLong("id", entity.Id.Id);
                    cmd.SetLong("blockColor", color.PackedValue);

                    ClientContext.OverlayController.AddCommand(cmd);
                };

                _current.OnClose += () => {
                    var cmd = BlobAllocator.Blob(true);
                    cmd.SetString("action", ChangeBlockColorCommand.KindCode());
                    cmd.SetLong("id", entity.Id.Id);

                    ClientContext.OverlayController.AddCommand(cmd);

                    ClientContext.WebOverlayRenderer.ReleaseInputControl();
                };

                _current.Show();

                ClientContext.WebOverlayRenderer.AcquireInputControl();
            }
        }

        

        public void Stop() { }
        public void Pause() { }
        public void Resume() { }
    }
}
