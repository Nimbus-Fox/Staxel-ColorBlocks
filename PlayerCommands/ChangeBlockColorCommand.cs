﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Items;
using Plukit.Base;
using Staxel.Core;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.ColorBlocks.PlayerCommands {
    class ChangeBlockColorCommand : IPlayerExtendedCommand {
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.command.changeBlockColor";
        }

        public void Invoke(PlayerEntityLogic logic, Entity entity, Blob config, Timestep timestep,
            EntityUniverseFacade facade) {
            if (entity.Inventory.ActiveItem().Item is PaintBlockItem paintBlock) {
                if (config.Contains("blockColor")) {
                    if (uint.TryParse(config.GetLong("blockColor").ToString(), out var col)) {
                        paintBlock.SetColor(ColorMath.FromRgba(col));
                        entity.Inventory.ItemStoreNeedsStorage();
                    }
                }

                paintBlock.StopControl = false;
            }
        }
    }
}
