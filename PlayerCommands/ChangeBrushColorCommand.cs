﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Items;
using Plukit.Base;
using Staxel.Core;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.ColorBlocks.PlayerCommands {
    class ChangeBrushColorCommand : IPlayerExtendedCommand {
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.command.changePaintColor";
        }

        public void Invoke(PlayerEntityLogic logic, Entity entity, Blob config, Timestep timestep,
            EntityUniverseFacade facade) {
            if (entity.Inventory.ActiveItem().Item is PaintBrushItem paintBrush) {
                if (config.Contains("paintColor")) {
                    if (uint.TryParse(config.GetLong("paintColor").ToString(), out var col)) {
                        paintBrush.SetColor(ColorMath.FromRgba(col));
                        entity.Inventory.ItemStoreNeedsStorage();
                    }
                }

                paintBrush.StopControl = false;
            }
        }
    }
}
