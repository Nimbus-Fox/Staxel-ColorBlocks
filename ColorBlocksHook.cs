﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Components;
using NimbusFox.ColorBlocks.Items;
using NimbusFox.ColorBlocks.Patches;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.ColorBlocks {
    public class ColorBlocksHook : IModHookV4 {
        internal static ColorBlocksHook Instance;
        private static Dictionary<string, MatrixDrawable> MatrixCache = new Dictionary<string, MatrixDrawable>();

        internal KitsuneCore.V1.KitsuneCore KsCore = new KitsuneCore.V1.KitsuneCore("NimbusFox", "Color Blocks");
        private List<Vector3I> _blackList = new List<Vector3I>();

        private bool _checking = false;

        static ColorBlocksHook() {
            ModHelper.CheckModIsInstalled("Color Blocks" ,"Kitsune Core");
        }

        public ColorBlocksHook() {
            Instance = this;
            TileConfigurationPatches.InitPatches();
        }

        internal static MatrixDrawable FetchDrawable(TileConfiguration configuration, Dictionary<Color, Color> colors) {
            var key = $"{configuration.Code}.{JsonConvert.SerializeObject(colors)}";
            if (MatrixCache.ContainsKey(key)) {
                return MatrixCache[key];
            }

            var matrix = configuration.Icon.Matrix();

            var component = configuration.Components.Select<PaintBlockComponent>().FirstOrDefault();

            var blob = BlobAllocator.Blob(true);

            var stream = GameContext.ContentLoader.ReadStream(configuration.Source);

            stream.Seek(0L, SeekOrigin.Begin);
            blob.LoadJsonStream(stream);

            var voxels = VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(blob.GetString("voxels")),
                blob.GetString("voxels"), Vector3I.Zero, Vector3I.MaxValue);

            if (component != default(PaintBlockComponent)) {
                foreach (var color in component.ColorCoords) {
                    var replace = colors[color.Key];
                    var coords = color.Value;

                    foreach (var coord in coords) {
                        voxels.ColorData[coord.X + coord.Z * (component.Voxels.Size.X) + coord.Y *
                                                   (component.Voxels.Size.X) * (component.Voxels.Size.Z)] = replace;
                    }
                }
            }

            MatrixCache.Add(key, voxels.BuildVertices().Matrix(matrix.MatrixValue));

            return MatrixCache[key];
        }

        internal static MatrixDrawable FetchDrawable(Item item, bool secondary, bool compact,
            Dictionary<Color, Color> colors) {
            var key = item.GetItemCode() + ".";
            if (secondary) {
                key += "secondary.";
            }

            if (compact) {
                key += "compact.";
            }

            key += JsonConvert.SerializeObject(colors);

            if (MatrixCache.ContainsKey(key)) {
                return MatrixCache[key];
            }

            var component = item.Configuration.Components.Select<PaintBlockComponent>().FirstOrDefault();

            if (compact) {
                var matrix = item.Configuration.CompactDrawable as MatrixDrawable;

                if (component != default(PaintBlockComponent)) {
                    foreach (var color in component.ColorCoords) {
                        var replace = colors[color.Key];
                        var coords = color.Value;

                        foreach (var coord in coords) {
                            component.Voxels.ColorData[coord.X + coord.Z * (component.Voxels.Size.X) + coord.Y *
                                 (component.Voxels.Size.X) * (component.Voxels.Size.Z)] = replace;
                        }
                    }
                }

                MatrixCache.Add(key, component.Voxels.BuildVertices().Matrix(matrix.MatrixValue));

                return MatrixCache[key];
            }

            if (secondary) {
                if (item.Configuration.SecondaryIcon == null) {
                    return null;
                }

                var matrix = item.Configuration.SecondaryIcon as MatrixDrawable;

                if (component != default(PaintBlockComponent)) {
                    foreach (var color in component.ColorCoords) {
                        var replace = colors[color.Key];
                        var coords = color.Value;

                        foreach (var coord in coords) {
                            component.Voxels.ColorData[coord.X + coord.Z * (component.Voxels.Size.X) + coord.Y *
                                                       (component.Voxels.Size.X) * (component.Voxels.Size.Z)] = replace;
                        }
                    }
                }

                MatrixCache.Add(key, component.Voxels.BuildVertices().Matrix(matrix.MatrixValue));

                return MatrixCache[key];
            }

            var mmatrix = item.Configuration.Icon as MatrixDrawable;

            if (component != default(PaintBlockComponent)) {
                foreach (var color in component.ColorCoords) {
                    var replace = colors[color.Key];
                    var coords = color.Value;

                    foreach (var coord in coords) {
                        component.Voxels.ColorData[coord.X + coord.Z * (component.Voxels.Size.X) + coord.Y *
                                                   (component.Voxels.Size.X) * (component.Voxels.Size.Z)] = replace;
                    }
                }
            }

            MatrixCache.Add(key, component.Voxels.BuildVertices().Matrix(mmatrix.MatrixValue));

            return MatrixCache[key];
        }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() {
        }
        public void GameContextInitializeInit() {
        }
        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() {
        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            //var players = new Lyst<Entity>();

            //universe.GetPlayers(players);

            //foreach (var player in players) {
            //    player.Inventory.EmptyInventory();
            //}
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            try {
                if (!_checking) {
                    _checking = true;
                    if (GameContext.ModdingController.CanRemoveTile(entity, location, accessFlags)) {
                        if (_blackList.Contains(location)) {
                            _blackList.Remove(location);
                            _checking = false;
                            return true;
                        }

                        if (entity.Inventory.ActiveItem().Item is ShovelTool &&
                            entity.PlayerEntityLogic?.CreativeModeEnabled() == true) {
                            return true;
                        }

                        if (KsCore.WorldManager.Universe.TryFetchTileStateEntityLogic(location, accessFlags, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                            out var logic)) {
                            if (logic is PaintBlockTileStateLogic paintLogic) {
                                var item =
                                    paintLogic.Configuration.MakeItem("nimbusfox.colorblocks.item.paintBlock") as
                                        PaintBlockItem;

                                item.Color = paintLogic.BlockColor;

                                ItemEntityBuilder.SpawnDroppedItem(entity, KsCore.WorldManager.Universe,
                                    new ItemStack(item, 1), location.ToVector3D(), Vector3D.Zero, Vector3D.Zero,
                                    SpawnDroppedFlags.None);

                                _blackList.Add(location);
                            }

                            if (logic is PaintBucketTileStateEntityLogic bucketLogic) {
                                var item =
                                    bucketLogic.Configuration.MakeItem("nimbusfox.colorblocks.item.paintBucket") as
                                        PaintBucketItem;

                                item.Color = bucketLogic.BlockColor;

                                ItemEntityBuilder.SpawnDroppedItem(entity, KsCore.WorldManager.Universe,
                                    new ItemStack(item, 1), location.ToVector3D(), Vector3D.Zero, Vector3D.Zero,
                                    SpawnDroppedFlags.None);

                                _blackList.Add(location);
                            }
                        }

                        _checking = false;
                    }
                }
            } catch {
                // ignore
            }



            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
