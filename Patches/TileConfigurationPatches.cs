﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Components;
using NimbusFox.ColorBlocks.Items;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.ColorBlocks.Patches {
    internal static class TileConfigurationPatches {
        public static void InitPatches() {
            //ColorBlocksHook.KsCore.PatchController.Add(typeof(TileConfiguration), "FetchDrawable", typeof(TileConfigurationPatches), "FetchDrawable");
        }

        private static bool FetchDrawable(
            TileConfiguration __instance,
            int scale,
            uint variant,
            VertexDrawableBuilder builder,
            ref Matrix4F matrix,
            ref Vector3F tileSubvoxelOffset,
            Vector4F[] lighting,
            Vector4F[] emissives,
            Vector3I worldPosition,
            uint? emissive) {
            if (__instance.TileStateKind != PaintBlockTileStateBuilder.KindCode() && __instance.TileStateKind != PaintBucketTileStateBuilder.KindCode()) {
                return true;
            }

            var tile = __instance.MakeTile(variant);

            if (ColorBlocksHook.Instance.KsCore.WorldManager.Universe.TileOffset(tile, worldPosition, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                out var offset, 1)) {
                tileSubvoxelOffset = offset;
            }

            return true;
        }
    }
}
