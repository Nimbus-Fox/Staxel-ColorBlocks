﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Items;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel.EntityActions;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.ColorBlocks.EntityActions {
    public class ColorBlockTilePlacerEntityAction : LocalTilePlacerEntityAction {
        private readonly string _animationGroup;

        public static string ColorKindCode() {
            return "nimbusfox.colorblocks.entityaction.colorBlockPlacerAction";
        }

        public override string Kind() {
            return ColorBlockTilePlacerEntityAction.ColorKindCode();
        }

        public ColorBlockTilePlacerEntityAction() {
            var stateHandlers =
                this.GetPrivateFieldValue<Dictionary<string, Action<Entity, EntityUniverseFacade>>>("_stateHandlers");
            var key = stateHandlers.First().Key;
            stateHandlers.Remove(key);
            var cancellableState = this.GetPrivateFieldValue<HashSet<string>>("_cancellableState");
            cancellableState.Remove(key);
            var stateUpdates =
                this.GetPrivateFieldValue<Dictionary<string, Action<Entity, EntityUniverseFacade>>>("_stateUpdates");
            stateUpdates.Remove(key);
            this.RegisterState("Loop", new Action<Entity, EntityUniverseFacade>(this.Loop), true, new Action<Entity, EntityUniverseFacade>(this.ColorLogicLoop), false);
            this._animationGroup = "staxel.emote.UndockItem";
        }

        private void ColorLogicLoop(Entity entity, EntityUniverseFacade facade) {
            if (!(entity.Inventory.ActiveItem().Item is PaintBlockItem tilePlacerItem))
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            else if (entity.Controller.ControlState(ControlAxis.Alt).Down) {
                entity.Logic.ActionFacade.SetActionCookie("canceled", true);
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            } else if (!entity.Controller.ControlState(ControlAxis.Main).Down && !entity.Controller.ControlState(ControlAxis.Alt).Down && entity.Logic.ActionFacade.GetActionCookie("canceled", false))
                entity.Logic.ActionFacade.SetActionCookie("canceled", false);
            else if (entity.Controller.ControlState(ControlAxis.Main).UpClick) {
                Vector3I position;
                Vector3I adjacent;
                if (entity.Logic.ItemFacade.LookingAtTile(out position, out adjacent)) {
                    entity.Logic.ActionFacade.SetActionCookie("endFailSafe", 1L);
                    tilePlacerItem.PlaceTile(entity, facade, position, adjacent);
                    if (facade.TryFetchTileStateEntityLogic(adjacent, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                        if (logic is PaintBlockTileStateLogic tileState) {
                            tileState.SetColor(tilePlacerItem.Color);
                        }

                        if (logic is PaintBucketTileStateEntityLogic bucketState) {
                            bucketState.SetColor(tilePlacerItem.Color);
                        }
                    }
                }
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            } else {
                if (entity.Controller.ControlState(ControlAxis.Main).Down)
                    return;
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            }
        }
    }
}
