﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Items;
using Plukit.Base;
using Staxel.EntityActions;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.ColorBlocks.EntityActions {
    public class LocalTilePlacerEntityAction : EntityActionDriver {
        private readonly string _animationGroup;

        public static string KindCode() {
            return "nimbusfox.colorblocks.entityaction.TilePlacerAction";
        }

        public LocalTilePlacerEntityAction() {
            this.RegisterState("Loop", new Action<Entity, EntityUniverseFacade>(this.Loop), true, new Action<Entity, EntityUniverseFacade>(this.LogicLoop), false);
            this.RegisterState("End", new Action<Entity, EntityUniverseFacade>(this.End), true, new Action<Entity, EntityUniverseFacade>(this.EndUpdate), false);
            this._animationGroup = "staxel.emote.UndockItem";
        }

        private void EndUpdate(Entity entity, EntityUniverseFacade facade) {
            if (!entity.Controller.ControlState(ControlAxis.Main).Down || entity.Controller.ControlState(ControlAxis.Alt).Down)
                return;
            this.Start(entity, facade);
        }

        public override string Kind() {
            return LocalTilePlacerEntityAction.KindCode();
        }

        public override void Start(Entity entity, EntityUniverseFacade facade) {
            if (entity.Logic.ActionFacade.GetActionCookie("canceled", false) || entity.Controller.ControlState(ControlAxis.Alt).Down)
                return;
            PaintBlockItem PaintBlockItem = entity.Inventory.ActiveItem().Item as PaintBlockItem;
            if (PaintBlockItem == null) {
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            } else {
                entity.Logic.ActionFacade.SetActionCookie("endFailSafe", 0L);
                this.RunAnimation(entity, this._animationGroup + ".begin", "Loop");
                PaintBlockItem.ShowTilePlacementPreview = true;
            }
        }

        protected override void OnCancel(Entity entity) {
            base.OnCancel(entity);
            PaintBlockItem PaintBlockItem = entity.Inventory.ActiveItem().Item as PaintBlockItem;
            if (PaintBlockItem != null)
                PaintBlockItem.ShowTilePlacementPreview = false;
            entity.Logic.ActionFacade.SetActionCookie("canceled", false);
            entity.Logic.ActionFacade.NoNextAction();
        }

        private void LogicLoop(Entity entity, EntityUniverseFacade facade) {
            PaintBlockItem PaintBlockItem = entity.Inventory.ActiveItem().Item as PaintBlockItem;
            if (PaintBlockItem == null)
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            else if (entity.Controller.ControlState(ControlAxis.Alt).Down) {
                entity.Logic.ActionFacade.SetActionCookie("canceled", true);
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            } else if (!entity.Controller.ControlState(ControlAxis.Main).Down && !entity.Controller.ControlState(ControlAxis.Alt).Down && entity.Logic.ActionFacade.GetActionCookie("canceled", false))
                entity.Logic.ActionFacade.SetActionCookie("canceled", false);
            else if (entity.Controller.ControlState(ControlAxis.Main).UpClick) {
                Vector3I position;
                Vector3I adjacent;
                if (entity.Logic.ItemFacade.LookingAtTile(out position, out adjacent)) {
                    entity.Logic.ActionFacade.SetActionCookie("endFailSafe", 1L);
                    PaintBlockItem.PlaceTile(entity, facade, position, adjacent);
                }
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            } else {
                if (entity.Controller.ControlState(ControlAxis.Main).Down)
                    return;
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            }
        }

        public void Loop(Entity entity, EntityUniverseFacade facade) {
            if (entity.Logic.ActionFacade.GetActionCookie("endFailSafe", 0L) == 1L)
                this.RunAnimation(entity, this._animationGroup + ".end", "End");
            else
                this.RunAnimation(entity, this._animationGroup + ".hold", nameof(Loop));
        }

        public void End(Entity entity, EntityUniverseFacade facade) {
            this.OnCancel(entity);
        }
    }
}
