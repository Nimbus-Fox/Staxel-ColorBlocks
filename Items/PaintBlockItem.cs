﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Effects;
using NimbusFox.ColorBlocks.EntityActions;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBlockItem : PaintBlockItemV2 {

        private PaintBlockItemBuilder _colorBuilder;

        public Color Color { get; internal set; } = Color.White;

        internal bool StopControl = false;

        internal bool UpdateDrawable = true;

        public MultipleMatrixDrawable IconDrawable { get; set; }
        public MultipleMatrixDrawable CompactDrawable { get; set; }
        public MultipleMatrixDrawable SecondaryDrawable { get; set; }

        internal PaintBlockItem(PaintBlockItemBuilder builder, Blob blob) :
            base(builder, blob) {
            _colorBuilder = builder;
            Kind = PaintBlockItemBuilder.KindCode();
        }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (StopControl) {
                return;
            }

            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic) && facade.ReadTile(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                        if (logic is PaintBucketTileStateEntityLogic paintLogic) {
                            if (main.DownClick) {
                                if (GameContext.ModdingController.CanReplaceTile(entity, target, tile,
                                    TileAccessFlags.None)) {
                                    paintLogic.SetColor(Color);
                                }
                                return;
                            }

                            if (alt.DownClick) {
                                SetColor(paintLogic.BlockColor);
                                entity.Inventory.ItemStoreNeedsStorage();
                                return;
                            }
                        }
                    }
                }
            }

            if (alt.DownClick && !main.Down) {
                var blob = BlobAllocator.Blob(true);
                blob.SetLong("blockColor", Color.PackedValue);
                blob.SetLong("target", entity.Id.Id);
                entity.Effects.Start(new EffectTrigger(ShowBlockColorPickerEffectBuilder.KindCode(), blob));
                StopControl = true;
            }

            if (!main.DownClick) {
                return;
            }
            entity.Logic.ActionFacade.NextAction(ColorBlockTilePlacerEntityAction.ColorKindCode());
        }

        public void SetColor(Color color) {
            Color = color;
        }

        public override void Store(Blob blob) {
            blob.SetLong("blockColor", Color.PackedValue);
            base.Store(blob);
        }

        public override void StorePersistenceData(Blob blob) {
            blob.SetLong("blockColor", Color.PackedValue);
            base.StorePersistenceData(blob);
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);
            if (blob.Contains("blockColor")) {
                if (uint.TryParse(blob.GetLong("blockColor").ToString(), out var col)) {
                    Color = ColorMath.FromRgba(col);
                    UpdateDrawable = true;
                }
            }
        }

        public override bool Same(Item item) {
            if (Item.NullItem == item) {
                return false;
            }

            if (item is PaintBlockItem paintBlock) {
                if (item.GetItemCode() == GetItemCode()) {
                    return paintBlock.Color == Color;
                }
            }

            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return _colorBuilder.Renderer;
        }

        public override bool TryResolveAltInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {

            if (entity.PlayerEntityLogic != null) {
                if (!entity.PlayerEntityLogic.DoingAction()) {
                    if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                        if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                            if (logic is PaintBucketTileStateEntityLogic) {
                                verb = "nimbusfox.colorblocks.action.getColor";
                                return true;
                            }
                        }

                        verb = "nimbusfox.colorblocks.action.setColor";
                        return true;
                    }
                }
            }

            return base.TryResolveAltInteractVerb(entity, facade, location, lookedAtTile, out verb);
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {

            if (entity.PlayerEntityLogic != null) {
                if (!entity.PlayerEntityLogic.DoingAction()) {
                    if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                        if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                            if (logic is PaintBucketTileStateEntityLogic) {
                                verb = "nimbusfox.colorblocks.action.setColor";
                                return true;
                            }
                        }

                        verb = "nimbusfox.colorblocks.action.place";
                        return true;
                    }
                }
            }
            return base.TryResolveMainInteractVerb(entity, facade, location, lookedAtTile, out verb);
        }
    }
}
