﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBrushItemBuilder : IItemBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() {
            Renderer = new PaintBlockItemRenderer();
        }

        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (spare is PaintBrushItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var brush = new PaintBrushItem(this, configuration);
            brush.Restore(configuration, blob);
            return brush;
        }

        public ItemRenderer Renderer { get; private set; }

        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.item.paintBrush";
        }
    }
}
