﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Components;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBucketItemBuilder : PaintBlockItemBuilder {
        public override string Kind() {
            return KindCode();
        }

        public new static string KindCode() {
            return "nimbusfox.colorblocks.item.paintBucket";
        }

        public override Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (!blob.Contains("tile")) {
                var component = configuration.Components.Select<PaintBlockComponent>().FirstOrDefault();

                if (component == default(PaintBlockComponent)) {
                    blob.SetString("tile", "nimbusfox.colorblocks.tile.paintBlock");
                } else {
                    blob.SetString("tile", component.Tile);
                }
            }
            if (!(spare is PaintBucketItem))
                return (Item)new PaintBucketItem(this, blob);
            spare.Restore(configuration, blob);
            return spare;
        }
    }
}
