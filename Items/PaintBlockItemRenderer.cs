﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Components;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Components;
using Plukit.Base;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Rendering;
using Helpers = NimbusFox.KitsuneCore.Helpers;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBlockItemRenderer : ItemRenderer {

        public override void Render(
            Item item,
            DeviceContext graphics,
            ref Matrix4F itemMatrix,
            Vector3D playerPosition,
            Vector3D renderOrigin,
            ref Matrix4F matrix,
            Timestep renderTimestep,
            string animation,
            float animationCycle,
            RenderMode renderMode) {
            Vector3F vector3F1 = (playerPosition - renderOrigin).ToVector3F();
            float num1 = MathHelper.Clamp(1f - item.Configuration.IconScale, 0.65f, 1f);
            TilePlacementComponent orDefault = item.Configuration.Components.GetOrDefault<TilePlacementComponent>();
            Vector3F vector3F2 = item is PaintBrushItem ? item.Configuration.InHandOffset : orDefault == null ? Vector3F.Zero : orDefault.InHandOffset;
            Matrix4F matrix2 = item is PaintBrushItem
                ?
                Matrix.CreateFromYawPitchRoll(item.Configuration.InHandRotation.X, item.Configuration.InHandRotation.Y,
                    item.Configuration.InHandRotation.Z).ToMatrix4F()
                : orDefault == null
                    ? Matrix4F.Identity
                    : orDefault.InHandRotationMatrix;
            double num2 = orDefault == null ? 0.5 : orDefault.InHandScale;
            Matrix4F matrix4F1 = item is PaintBrushItem ? Matrix4F.CreateScale(item.Configuration.InHandScale)
                : Matrix4F.CreateScale(num1 * (float)num2);
            matrix4F1 = matrix4F1.Multiply(ref matrix2);
            Matrix4F matrix4F2 = matrix4F1.Translate(vector3F2 * num1 * (float)num2);
            matrix4F2 = matrix4F2.Multiply(ref itemMatrix);
            matrix4F2 = matrix4F2.Translate(vector3F1);
            Matrix4F matrix1 = matrix4F2.Multiply(ref matrix);
            UpdateDrawable(item);
            if (item is PaintBrushItem paintBrush) {
                paintBrush.IconDrawable?.Render(graphics, ref matrix1);
            }

            if (item is PaintBlockItem paintBlock) {
                paintBlock.IconDrawable?.Render(graphics, ref matrix1);
            }

            //item.Configuration.Icon.Render(graphics, ref matrix1);
        }

        public override void RenderIcon(Item item, DeviceContext graphics, ref Matrix4F itemMatrix) {
            UpdateDrawable(item);
            if (_isDisposed) {
                throw new ObjectDisposedException(nameof(PaintBlockItemRenderer));
            }

            if (item is PaintBrushItem paintBrush) {
                paintBrush.IconDrawable?.Render(graphics, ref itemMatrix);
            }

            if (item is PaintBlockItem paintBlock) {
                paintBlock.IconDrawable?.Render(graphics, ref itemMatrix);
            }
            //item.Configuration.Icon.Render(graphics, ref itemMatrix);
        }

        public override void RenderInWorldFullSized(Item item, DeviceContext graphics, ref Matrix4F matrix, bool compact, bool secondary) {
            UpdateDrawable(item);
            if (_isDisposed) {
                throw new ObjectDisposedException(nameof(PaintBlockItemRenderer));
            }

            if (item is PaintBrushItem paintBrush) {
                if (compact) {
                    paintBrush.CompactDrawable?.Render(graphics, ref matrix);
                } else if (secondary && item.Configuration.SecondaryIcon != null) {
                    paintBrush.SecondaryDrawable?.Render(graphics, ref matrix);
                } else {
                    paintBrush.IconDrawable?.Render(graphics, ref matrix);
                }
            }

            if (item is PaintBlockItem paintBlock) {
                if (compact) {
                    paintBlock.CompactDrawable?.Render(graphics, ref matrix);
                } else if (secondary && item.Configuration.SecondaryIcon != null) {
                    paintBlock.SecondaryDrawable?.Render(graphics, ref matrix);
                } else {
                    paintBlock.IconDrawable?.Render(graphics, ref matrix);
                }
            }
        }

        private static readonly Dictionary<Color, Color> Recolors = new Dictionary<Color, Color>();
        
        private void UpdateDrawable(Item item) {
            if (item is PaintBlockItem paintBlock) {
                if (paintBlock.UpdateDrawable) {

                    var component = paintBlock.Configuration.Components.Select<VoxelRecolorComponent>().FirstOrDefault();

                    if (component != default(VoxelRecolorComponent)) {
                        Recolors.Clear();
                        foreach (var col in component.ColorCoords.Keys) {
                            if (col.R != col.G || col.G != col.B) {
                                Recolors.Add(col, col);
                                continue;
                            }
                            var shade = new Color(byte.MaxValue - col.R, byte.MaxValue - col.G, byte.MaxValue - col.B);
                            Recolors.Add(col, new Color(
                                Helpers.GetNewByte(paintBlock.Color.R, shade.R),
                                Helpers.GetNewByte(paintBlock.Color.G, shade.G),
                                Helpers.GetNewByte(paintBlock.Color.B, shade.B)
                            ));
                        }
                        paintBlock.CompactDrawable = VoxelRecolor.FetchDrawable(paintBlock, Recolors,
                            false, true);

                        paintBlock.SecondaryDrawable = VoxelRecolor.FetchDrawable(paintBlock, Recolors,
                            true, false);

                        paintBlock.IconDrawable = VoxelRecolor.FetchDrawable(paintBlock, Recolors,
                            false, false);
                    }

                    paintBlock.UpdateDrawable = false;
                }
            }

            if (item is PaintBrushItem paintBrush) {
                if (paintBrush.UpdateDrawable) {

                    var component = paintBrush.Configuration.Components.Select<VoxelRecolorComponent>().FirstOrDefault();

                    if (component != default(VoxelRecolorComponent)) {
                        Recolors.Clear();
                        foreach (var col in component.ColorCoords.Keys) {
                            if (col.R != col.G || col.G != col.B) {
                                Recolors.Add(col, col);
                                continue;
                            }
                            var shade = new Color(byte.MaxValue - col.R, byte.MaxValue - col.G, byte.MaxValue - col.B);
                            Recolors.Add(col, new Color(
                                Helpers.GetNewByte(paintBrush.Color.R, shade.R),
                                Helpers.GetNewByte(paintBrush.Color.G, shade.G),
                                Helpers.GetNewByte(paintBrush.Color.B, shade.B)
                            ));
                        }
                        paintBrush.CompactDrawable = VoxelRecolor.FetchDrawable(paintBrush, Recolors,
                            false, true);

                        paintBrush.SecondaryDrawable = VoxelRecolor.FetchDrawable(paintBrush, Recolors,
                            true, false);

                        paintBrush.IconDrawable = VoxelRecolor.FetchDrawable(paintBrush, Recolors,
                            false, false);
                    }
                    
                    paintBrush.UpdateDrawable = false;
                }
            }
        }
    }
}
