﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBucketItem : PaintBlockItem {
        internal PaintBucketItem(PaintBlockItemBuilder builder, Blob blob) : base(builder, blob) {
            Kind = PaintBucketItemBuilder.KindCode();
        }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (StopControl) {
                return;
            }

            if (main.DownClick || main.Down) {
                if (entity.PlayerEntityLogic != null) {
                    if (entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent)) {
                        if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tileStateLogic)) {
                            if (tileStateLogic is PaintBlockTileStateLogic) {
                                VectorCubeI region;
                                if (adjacent.Y > target.Y || adjacent.Y < target.Y) {
                                    region = new VectorCubeI(target - new Vector3I(1, 0, 1), target + new Vector3I(1, 0, 1));
                                } else if (adjacent.X > target.X || adjacent.X < target.X) {
                                    region = new VectorCubeI(target - new Vector3I(0, 1, 1), target + new Vector3I(0, 1, 1));
                                } else {
                                    region = new VectorCubeI(target - new Vector3I(1, 1, 0), target + new Vector3I(1, 1, 0));
                                }

                                Helpers.VectorLoop(region, (x, y, z) => {
                                    var current = new Vector3I(x, y, z);
                                    if (facade.TryFetchTileStateEntityLogic(current, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                            out var logic) && facade.ReadTile(current, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                            out var tile)) {
                                        if (logic is PaintBlockTileStateLogic paintLogic) {
                                            if (main.DownClick) {
                                                if (GameContext.ModdingController.CanReplaceTile(entity, current, tile,
                                                    TileAccessFlags.None)) {
                                                    paintLogic.SetColor(Color);
                                                }
                                            }
                                        }
                                    }
                                });
                                return;
                            }
                        }
                    }
                }
            }

            base.Control(entity, facade, main, alt);
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                        if (logic is PaintBlockTileStateLogic) {
                            verb = "nimbusfox.colorblocks.action.paint";
                            return true;
                        }
                    }
                }
            }
            return base.TryResolveMainInteractVerb(entity, facade, location, lookedAtTile, out verb);
        }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent)) {
                    if (universe.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tileStateLogic)) {
                        if (tileStateLogic is PaintBlockTileStateLogic) {
                            VectorCubeI region;
                            if (adjacent.Y > target.Y || adjacent.Y < target.Y) {
                                region = new VectorCubeI(target - new Vector3I(1, 0, 1),
                                    target + new Vector3I(1, 0, 1));
                            } else if (adjacent.X > target.X || adjacent.X < target.X) {
                                region = new VectorCubeI(target - new Vector3I(0, 1, 1),
                                    target + new Vector3I(0, 1, 1));
                            } else {
                                region = new VectorCubeI(target - new Vector3I(1, 1, 0),
                                    target + new Vector3I(1, 1, 0));
                            }

                            Helpers.VectorLoop(region, (x, y, z) => {
                                if (universe.TryFetchTileStateEntityLogic(new Vector3I(x, y, z), TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                    out var targetLogic)) {
                                    if (targetLogic is PaintBlockTileStateLogic paintBlockLogic) {
                                        previews.Add(new Vector3I(x, y, z),
                                            paintBlockLogic.Configuration.MakeTile());
                                    }
                                }
                            });

                            return true;
                        }
                    }
                }
            }

            return base.PlacementTilePreview(avatar, entity, universe, previews);
        }
    }
}
