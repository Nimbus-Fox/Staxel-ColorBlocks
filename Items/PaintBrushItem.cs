﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Components.Item;
using NimbusFox.ColorBlocks.Effects;
using NimbusFox.ColorBlocks.TileStateEntities;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBrushItem : Item {

        private PaintBrushItemBuilder _builder;

        public Color Color { get; private set; } = Color.White;

        internal bool StopControl = false;

        internal bool UpdateDrawable = true;

        private PaintBrushComponent _brushComponent;

        public MultipleMatrixDrawable IconDrawable { get; set; }
        public MultipleMatrixDrawable CompactDrawable { get; set; }
        public MultipleMatrixDrawable SecondaryDrawable { get; set; }

        public PaintBrushItem(PaintBrushItemBuilder builder, ItemConfiguration configuration) : base(builder.Kind()) {
            _builder = builder;
            Configuration = configuration;

            var component = Configuration.Components.Select<PaintBrushComponent>().FirstOrDefault();

            _brushComponent = component ?? new PaintBrushComponent();
        }
        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (StopControl) {
                return;
            }

            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic) && facade.ReadTile(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                        if (logic is PaintBucketTileStateEntityLogic paintLogic) {
                            if (main.DownClick) {
                                if (GameContext.ModdingController.CanReplaceTile(entity, target, tile,
                                    TileAccessFlags.None)) {
                                    paintLogic.SetColor(Color);
                                }
                                return;
                            }

                            if (alt.DownClick) {
                                SetColor(paintLogic.BlockColor);
                                entity.Inventory.ItemStoreNeedsStorage();
                                return;
                            }
                        }
                    }
                }
            }

            if (alt.DownClick && !main.Down) {
                var blob = BlobAllocator.Blob(true);
                blob.SetLong("paintColor", Color.PackedValue);
                blob.SetLong("target", entity.Id.Id);
                entity.Effects.Start(new EffectTrigger(ShowBrushColorPickerEffectBuilder.KindCode(), blob));
                StopControl = true;
            }

            if (!main.DownClick && !main.Down) {
                return;
            }

            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tileLogic)) {
                        if (tileLogic is PaintBlockTileStateLogic) {
                            VectorCubeI region;
                            if (adjacent.Y > target.Y || adjacent.Y < target.Y) {
                                var direction = entity.PlayerEntityLogic.Heading().GetDirection();
                                if (direction == Compass.NORTH || direction == Compass.SOUTH) {
                                    region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.Y, 0, _brushComponent.Size.X),
                                        target + new Vector3I(_brushComponent.Size.Y, 0, _brushComponent.Size.X));
                                } else {
                                    region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.X, 0, _brushComponent.Size.Y),
                                        target + new Vector3I(_brushComponent.Size.X, 0, _brushComponent.Size.Y));
                                }
                            } else if (adjacent.X > target.X || adjacent.X < target.X) {
                                region = new VectorCubeI(target - new Vector3I(0, _brushComponent.Size.Y, _brushComponent.Size.X),
                                    target + new Vector3I(0, _brushComponent.Size.Y, _brushComponent.Size.X));
                            } else {
                                region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.X, _brushComponent.Size.Y, 0),
                                    target + new Vector3I(_brushComponent.Size.X, _brushComponent.Size.Y, 0));
                            }

                            Helpers.VectorLoop(region, (x, y, z) => {
                                if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                        out var logic) && facade.ReadTile(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                        out var tile)) {
                                    if (logic is PaintBlockTileStateLogic paintLogic) {
                                        if (main.DownClick) {
                                            if (GameContext.ModdingController.CanReplaceTile(entity, target, tile,
                                                TileAccessFlags.None)) {
                                                paintLogic.SetColor(Color);
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }

        protected override void AssignFrom(Item item) {
            if (item is PaintBrushItem brush) {
                SetColor(brush.Color);
            }
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public void SetColor(Color color) {
            Color = color;
        }

        public override void Store(Blob blob) {
            blob.SetLong("paintColor", Color.PackedValue);
            base.Store(blob);
        }

        public override void StorePersistenceData(Blob blob) {
            blob.SetLong("paintColor", Color.PackedValue);
            base.StorePersistenceData(blob);
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);
            if (blob.Contains("paintColor")) {
                if (uint.TryParse(blob.GetLong("paintColor").ToString(), out var col)) {
                    Color = ColorMath.FromRgba(col);
                    UpdateDrawable = true;
                }
            }
        }

        public override bool Same(Item item) {
            if (Item.NullItem == item) {
                return false;
            }

            if (item is PaintBrushItem paintBrush) {
                if (item.GetItemCode() == GetItemCode()) {
                    return paintBrush.Color == Color;
                }
            }

            return false;
        }



        public override bool TryResolveAltInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {

            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                        if (logic is PaintBucketTileStateEntityLogic) {
                            verb = "nimbusfox.colorblocks.action.getColor";
                            return true;
                        }
                    }

                    verb = "nimbusfox.colorblocks.action.setColor";
                    return true;
                }
            }

            return base.TryResolveAltInteractVerb(entity, facade, location, lookedAtTile, out verb);
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    if (facade.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                        if (logic is PaintBucketTileStateEntityLogic) {
                            verb = "nimbusfox.colorblocks.action.setColor";
                            return true;
                        }

                        if (logic is PaintBlockTileStateLogic) {
                            verb = "nimbusfox.colorblocks.action.paint";
                            return true;
                        }
                    }
                }
            }
            return base.TryResolveMainInteractVerb(entity, facade, location, lookedAtTile, out verb);
        }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            if (entity.PlayerEntityLogic != null) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent)) {
                    if (universe.TryFetchTileStateEntityLogic(target, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tileStateLogic)) {
                        if (tileStateLogic is PaintBlockTileStateLogic) {
                            VectorCubeI region;
                            if (adjacent.Y > target.Y || adjacent.Y < target.Y) {
                                var direction = entity.PlayerEntityLogic.Heading().GetDirection();
                                if (direction == Compass.NORTH || direction == Compass.SOUTH) {
                                    region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.X, 0, _brushComponent.Size.Y),
                                        target + new Vector3I(_brushComponent.Size.X, 0, _brushComponent.Size.Y));
                                } else {
                                    region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.Y, 0, _brushComponent.Size.X),
                                        target + new Vector3I(_brushComponent.Size.Y, 0, _brushComponent.Size.X));
                                }
                            } else if (adjacent.X > target.X || adjacent.X < target.X) {
                                region = new VectorCubeI(target - new Vector3I(0, _brushComponent.Size.Y, _brushComponent.Size.X),
                                    target + new Vector3I(0, _brushComponent.Size.Y, _brushComponent.Size.X));
                            } else {
                                region = new VectorCubeI(target - new Vector3I(_brushComponent.Size.X, _brushComponent.Size.Y, 0),
                                    target + new Vector3I(_brushComponent.Size.X, _brushComponent.Size.Y, 0));
                            }

                            Helpers.VectorLoop(region, (x, y, z) => {
                                if (universe.TryFetchTileStateEntityLogic(new Vector3I(x, y, z), TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                    out var targetLogic)) {
                                    if (targetLogic is PaintBlockTileStateLogic paintBlockLogic) {
                                        previews.Add(new Vector3I(x, y, z),
                                            paintBlockLogic.Configuration.MakeTile());
                                    }
                                }
                            });

                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
