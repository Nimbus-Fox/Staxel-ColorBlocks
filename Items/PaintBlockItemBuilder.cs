﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ColorBlocks.Components;
using NimbusFox.KitsuneCore.V1.Components;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBlockItemBuilder : IItemBuilder {
        public void Dispose() { }

        public void Load() {
            Renderer = new PaintBlockItemRenderer();
        }

        public virtual Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (!blob.Contains("tile")) {
                blob.SetString("tile", "nimbusfox.colorblocks.tile.paintBlock");
            }

            var component = configuration?.Components.Select<VoxelRecolorComponent>().FirstOrDefault();

            if (component != null) {
                blob.SetString("tile", component.Tile);
            } else {
                if (spare is PaintBlockItem sparePaint) {
                    blob.SetString("tile", sparePaint.TileConfig.Code);
                }
            }

            if (!(spare is PaintBlockItem))
                return (Item)new PaintBlockItem(this, blob);
            spare.Restore(configuration, blob);
            return spare;
        }

        public ItemRenderer Renderer { get; private set; }

        public virtual string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.item.paintBlock";
        }
    }
}
