﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;
using Staxel.AutoTiling;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Effects;
using Staxel.EntityActions;
using Staxel.Gatherables;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.Translation;

namespace NimbusFox.ColorBlocks.Items {
    public class PaintBlockItemV2 : Item {
        protected TileConfiguration _tileConfiguration;
        private bool _placementProblem;
        private Stopwatch _timeout;
        private Lyst<LingerPreviewData> _lingerPreviews;
        private readonly PaintBlockItemBuilder _builder;

        public TileConfiguration TileConfig {
            get {
                return _tileConfiguration;
            }
        }

        internal PaintBlockItemV2(PaintBlockItemBuilder builder, Blob blob)
          : base(builder.Kind()) {
            _builder = builder;
            Restore(null, blob);
        }

        protected PaintBlockItemV2(IItemBuilder builder, Blob blob)
          : base(builder.Kind()) {
            Restore(null, blob);
        }

        public static string KindCode() {
            return "nimbusfox.staxel.placer";
        }

        private PaintBlockItemV2(PaintBlockItemV2 self)
          : base(self.Kind) {
            _builder = self._builder;
            AssignFrom(self);
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override bool HasAltAction() {
            return false;
        }

        public override void Control(
          Entity entity,
          EntityUniverseFacade facade,
          ControlState main,
          ControlState alt) {
            if (!main.DownClick)
                return;
            entity.Logic.ActionFacade.NextAction(TilePlacerEntityAction.KindCode());
        }

        public override bool Same(Item item) {
            if (!(item is PaintBlockItemV2 paintBlockItemV2))
                return false;
            return _tileConfiguration.Code == paintBlockItemV2._tileConfiguration.Code;
        }

        public Item Clone(PaintBlockItemV2 spare) {
            if (spare == null || spare.Kind != Kind)
                return new PaintBlockItemV2(this);
            spare.AssignFrom(this);
            return spare;
        }

        public override bool MatchesSpecification(Blob blob) {
            if (!blob.IsMaster())
                throw new Exception();
            return blob.Contains("kind") && blob.GetString("kind") == Kind && blob.GetString("tile") == GetItemCode();
        }

        protected override void AssignFrom(Item item) {
            PaintBlockItemV2 paintBlockItemV2 = (PaintBlockItemV2)item;
            Configuration = paintBlockItemV2.Configuration;
            _tileConfiguration = paintBlockItemV2._tileConfiguration;
            ShowTilePlacementPreview = paintBlockItemV2.ShowTilePlacementPreview;
            _placementProblem = paintBlockItemV2._placementProblem;
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade facade) {
        }

        public override string GetItemCode() {
            return _tileConfiguration.Code;
        }

        public override string GetItemTranslation(LanguageDatabase lang) {
            return _tileConfiguration.GetTranslatedName(lang);
        }

        public override string GetItemDescription(LanguageDatabase lang) {
            return _tileConfiguration.GetTranslatedDescription(lang);
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            blob.SetString("tile", _tileConfiguration.Code);
        }

        public override bool TilePlacementProblem() {
            return _placementProblem;
        }

        public override bool PlacementTilePreview(
          AvatarController avatar,
          Entity entity,
          Universe universe,
          Vector3IMap<Tile> previews) {
            previews.Clear();
            Vector3I cursor;
            Vector3I adjacent;
            if (!avatar.LookingAtTile(out cursor, out adjacent))
                return false;
            _placementProblem = false;
            if (avatar.ShowTilePlacementPreview()) {
                long num = _tileConfiguration.PlacementPreviewVariant;
                if (num < 0L)
                    num = avatar.PlacementVariant();
                uint variant = _tileConfiguration.UpdateVariantWithNewRotation(_tileConfiguration.MaskPlacementVariant((uint)num), avatar.PlacementRotation());
                Tile tile = _tileConfiguration.MakeTile(variant);
                FindCompoundSpot(universe, tile, ref adjacent);
                if (_lingerPreviews == null)
                    _lingerPreviews = new Lyst<LingerPreviewData>();
                else
                    _lingerPreviews.Clear();
                Vector3IMap<Tile> instance = Allocator.TileVector3IMap.Allocate();
                if (TileProcessLogic.AutoTileProcess(_tileConfiguration, (int)avatar.PlacementRotation(), cursor, adjacent, variant, universe, TileAccessFlags.None, ChunkFetchKind.LivingWorld, instance)) {
                    LingerPreviewData lingerPreviewData1;
                    foreach (KeyValuePair<Vector3I, Tile> keyValuePair in instance) {
                        if (keyValuePair.Key == Vector3I.Zero) {
                            Lyst<LingerPreviewData> lingerPreviews = _lingerPreviews;
                            lingerPreviewData1 = new LingerPreviewData();
                            lingerPreviewData1.Offset = keyValuePair.Key;
                            lingerPreviewData1.Adjacent = adjacent;
                            lingerPreviewData1.Cursor = cursor;
                            lingerPreviewData1.Rotation = avatar.PlacementRotation();
                            lingerPreviewData1.Variant = avatar.PlacementVariant();
                            lingerPreviewData1.Replace = false;
                            lingerPreviewData1.Tile = keyValuePair.Value;
                            LingerPreviewData lingerPreviewData2 = lingerPreviewData1;
                            lingerPreviews.Add(lingerPreviewData2);
                        } else {
                            Lyst<LingerPreviewData> lingerPreviews = _lingerPreviews;
                            lingerPreviewData1 = new LingerPreviewData();
                            lingerPreviewData1.Offset = keyValuePair.Key;
                            lingerPreviewData1.Adjacent = adjacent;
                            lingerPreviewData1.Cursor = cursor;
                            lingerPreviewData1.Rotation = avatar.PlacementRotation();
                            lingerPreviewData1.Variant = avatar.PlacementVariant();
                            lingerPreviewData1.Replace = true;
                            lingerPreviewData1.Tile = keyValuePair.Value;
                            LingerPreviewData lingerPreviewData2 = lingerPreviewData1;
                            lingerPreviews.Add(lingerPreviewData2);
                        }
                    }
                    instance.Clear();
                    Allocator.TileVector3IMap.Release(ref instance);
                } else
                    _lingerPreviews.Add(new LingerPreviewData() {
                        Offset = Vector3I.Zero,
                        Adjacent = adjacent,
                        Cursor = cursor,
                        Rotation = avatar.PlacementRotation(),
                        Variant = avatar.PlacementVariant(),
                        Replace = false,
                        Tile = tile
                    });
                if (_timeout == null)
                    _timeout = Stopwatch.StartNew();
                else
                    _timeout.Restart();
            }
            if (_lingerPreviews != null && _lingerPreviews.Count > 0) {
                if (!CanPlaceCheck(universe, entity))
                    _placementProblem = true;
                if (_timeout.ElapsedMilliseconds > Constants.TilePlaceItemLingerTimeoutMillis) {
                    _lingerPreviews.Clear();
                } else {
                    for (int index = 0; index < _lingerPreviews.Count; ++index) {
                        LingerPreviewData lingerPreview = _lingerPreviews[index];
                        Tile result;
                        if (universe.ReadTile(lingerPreview.Offset + lingerPreview.Adjacent, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out result) && (result.Configuration.CanonicalConfiguration == lingerPreview.Tile.Configuration.CanonicalConfiguration || adjacent != lingerPreview.Adjacent || (cursor != lingerPreview.Cursor || (int)avatar.PlacementVariant() != (int)lingerPreview.Variant) || (int)avatar.PlacementRotation() != (int)lingerPreview.Rotation)) {
                            if (index < _lingerPreviews.Count - 1)
                                _lingerPreviews[index] = _lingerPreviews[_lingerPreviews.Count - 1];
                            _lingerPreviews.RemoveAt(_lingerPreviews.Count - 1);
                            --index;
                        }
                    }
                }
                foreach (LingerPreviewData lingerPreview in _lingerPreviews)
                    previews[lingerPreview.Offset + lingerPreview.Adjacent] = lingerPreview.Tile;
            }
            return true;
        }

        public void PlaceTile(
          Entity entity,
          EntityUniverseFacade facade,
          Vector3I cursor,
          Vector3I adjacent) {
            uint rotation = entity.Logic.ItemFacade.TilePlacementRotation();
            uint variant1;
            if (_tileConfiguration.UseCustomRendering) {
                long num = _tileConfiguration.PlacementPreviewVariant;
                if (num < 0L)
                    num = entity.Logic.ItemFacade.TilePlacementVariant();
                variant1 = _tileConfiguration.MaskPlacementVariant((uint)num);
            } else
                variant1 = _tileConfiguration.MaskPlacementVariant(entity.Logic.ItemFacade.TilePlacementVariant());
            uint variant2 = _tileConfiguration.UpdateVariantWithNeighbours(_tileConfiguration.UpdateVariantWithNewRotation(variant1, rotation), facade, adjacent, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
            Tile tile = _tileConfiguration.MakeTile(variant2);
            FindCompoundSpot(facade, tile, ref adjacent);
            Vector3IMap<Tile> instance = Allocator.TileVector3IMap.Allocate();
            if (!TileProcessLogic.AutoTileProcess(_tileConfiguration, (int)rotation, cursor, adjacent, variant2, facade, TileAccessFlags.None, ChunkFetchKind.LivingWorld, instance))
                instance.Add(Vector3I.Zero, tile);
            PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
            foreach (KeyValuePair<Vector3I, Tile> keyValuePair in instance) {
                bool flag1 = !(keyValuePair.Key == Vector3I.Zero) ? facade.CanReplaceTile(entity, adjacent + keyValuePair.Key, keyValuePair.Value, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId) : facade.CanPlaceTile(entity, adjacent + keyValuePair.Key, keyValuePair.Value, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                bool flag2 = false;
                if (GameContext.GatheringDatabase.IsTileCatchable(keyValuePair.Value)) {
                    flag1 = true;
                    flag2 = true;
                }
                if (flag1) {
                    if (flag2)
                        PerformCatchableReplace(facade, entity, adjacent + keyValuePair.Key, keyValuePair.Value, TileAccessFlags.None);
                    else
                        PerformReplace(facade, entity, adjacent + keyValuePair.Key, keyValuePair.Value, TileAccessFlags.None);
                    Vector3D particleOffset = keyValuePair.Value.Configuration.TileCenter(adjacent + keyValuePair.Key, keyValuePair.Value.Variant());
                    BaseEffects.PlaceTileEffect(entity, keyValuePair.Value.Configuration.PlaceSoundGroup, BaseEffects.PlaceParticleCode, particleOffset, keyValuePair.Value.Configuration.FaceColor[2]);
                    entity.Achievements.RegisterPlaceItemProgress(this);
                    if (!playerEntityLogic.CreativeModeEnabled())
                        entity.Inventory.ConsumeItem(new ItemStack(this, 1), "");
                }
            }
            instance.Clear();
            Allocator.TileVector3IMap.Release(ref instance);
            ShowTilePlacementPreview = false;
        }

        protected virtual void PerformReplace(
          EntityUniverseFacade facade,
          Entity entity,
          Vector3I location,
          Tile tile,
          TileAccessFlags accessFlags) {
            facade.ReplaceTile(entity, location, tile, null, accessFlags, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
        }

        private void PerformCatchableReplace(
          EntityUniverseFacade facade,
          Entity entity,
          Vector3I location,
          Tile tile,
          TileAccessFlags accessFlags) {
            ItemConfiguration catchable;
            if (GameContext.GatheringDatabase.TryGetCatchableByTile(tile, out catchable)) {
                Blob blob;
                if (!ServerContext.TileBlobDatabase.TryGet("gathering", location, out blob) || GameContext.GatheringDatabase.IsTileBlobCatchable(blob)) {
                    GatherableComponent.CatchableConfig catching = catchable.Components.Get<GatherableComponent>().Catching;
                    GameContext.GatheringDatabase.InitialiseCatchableState(location, facade.DayNightCycle().Day, true, catching.SpawnChance, true);
                } else
                    Logger.WriteLine("Location " + location + " is reserved for a different gatherable");
            }
            PerformReplace(facade, entity, location, tile, accessFlags);
        }

        public void FindCompoundSpot(
          EntityUniverseFacade universe,
          Tile tile,
          ref Vector3I corePosition) {
            Tile[] tempTiles;
            if (!tile.Configuration.CompoundComponent || !universe.ReadTile27Region(corePosition - Vector3I.One, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out tempTiles))
                return;
            Vector3I lower;
            Vector3I upper;
            tile.Configuration.CompoundRelativeBounds(tile, out lower, out upper);
            bool flag1 = !tempTiles[4].Configuration.Open;
            bool flag2 = !tempTiles[22].Configuration.Open;
            if (flag1 & flag2)
                flag1 = flag2 = false;
            if (flag2)
                corePosition += new Vector3I(0, -upper.Y, 0);
            if (flag1)
                corePosition += new Vector3I(0, -lower.Y, 0);
            bool flag3 = !tempTiles[12].Configuration.Open;
            bool flag4 = !tempTiles[14].Configuration.Open;
            if (flag3 & flag4)
                flag3 = flag4 = false;
            if (flag4)
                corePosition += new Vector3I(-upper.X, 0, 0);
            if (flag3)
                corePosition += new Vector3I(-lower.X, 0, 0);
            bool flag5 = !tempTiles[10].Configuration.Open;
            bool flag6 = !tempTiles[16].Configuration.Open;
            if (flag5 & flag6)
                flag5 = flag6 = false;
            if (flag6)
                corePosition += new Vector3I(0, 0, -upper.Z);
            if (!flag5)
                return;
            corePosition += new Vector3I(0, 0, -lower.Z);
        }

        public bool MatchMaterialConfiguration(TileConfiguration configuration) {
            return _tileConfiguration.Code == configuration.Code;
        }

        public bool MatchMaterialConfigurationCode(string code) {
            return _tileConfiguration.Code == code;
        }

        private bool CanPlaceCheck(Universe universe, Entity entity) {
            bool flag = true;
            if (_lingerPreviews != null) {
                foreach (LingerPreviewData lingerPreview in _lingerPreviews) {
                    if (lingerPreview.Replace) {
                        if (!universe.CanReplaceTile(entity, lingerPreview.Offset + lingerPreview.Adjacent, lingerPreview.Tile, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId)) {
                            flag = false;
                            _placementProblem = true;
                            break;
                        }
                    } else if (!universe.CanPlaceTile(entity, lingerPreview.Offset + lingerPreview.Adjacent, lingerPreview.Tile, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId)) {
                        flag = false;
                        _placementProblem = true;
                        break;
                    }
                }
            }
            return flag;
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            string tileCode = blob.GetString("tile");
            _prevConfigRevision = (int)blob.GetLong("configRevision", 0L);
            _tileConfiguration = GameContext.TileDatabase.GetTileConfiguration(tileCode);
            Configuration = _tileConfiguration.ItemConfiguration;
            base.Restore(Configuration, blob);
        }

        public override Plukit.Base.Components GetComponents() {
            return _tileConfiguration.Components;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        private struct LingerPreviewData : IEquatable<LingerPreviewData> {
            public Vector3I Offset;
            public Vector3I Adjacent;
            public Vector3I Cursor;
            public uint Rotation;
            public uint Variant;
            public bool Replace;
            public Tile Tile;

            public bool Equals(LingerPreviewData other) {
                if (Offset == other.Offset && Adjacent == other.Adjacent && (Cursor == other.Cursor && (int)Rotation == (int)other.Rotation) && (int)Variant == (int)other.Variant)
                    return Tile == other.Tile;
                return false;
            }

            public override bool Equals(object obj) {
                return Equals((LingerPreviewData)obj);
            }

            public override int GetHashCode() {
                return Offset.GetHashCode() ^ 13 * Adjacent.GetHashCode() ^ 39 * Cursor.GetHashCode() ^ (int)Rotation * 130 ^ (int)Variant * 1300 ^ Tile.GetHashCode() * 19;
            }
        }
    }
}
