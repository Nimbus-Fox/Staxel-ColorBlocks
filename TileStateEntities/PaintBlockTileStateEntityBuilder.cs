﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ColorBlocks.TileStateEntities {
    public class PaintBlockTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public void Load() {

        }

        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.colorblocks.tileStateEntity.colorBlock";

        public bool IsTileStateEntityKind() {
            return true;
        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new PaintBlockTileStateLogic(entity);
        }

        EntityPainter IEntityPainterBuilder.Instance() {
            return new PaintBlockTileStatePainter();
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Configuration.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
