﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ColorBlocks.TileStateEntities {
    class PaintBucketTileStateEntityLogic : TileStateEntityLogic {
        internal Color BlockColor { get; private set; } = Color.White;
        internal bool UpdateColor { get; set; } = true;
        private bool _needsStore = true;
        internal TileConfiguration Configuration;
        private Blob _constructArguments;

        public PaintBucketTileStateEntityLogic(Entity entity) : base(entity) {
        }
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {

        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration.Code != Configuration.Code) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.FetchBlob("location").GetVector3I();
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
            Configuration.ExportColor = Color.Transparent;
            _constructArguments = BlobAllocator.Blob(true);
            _constructArguments.AssignFrom(arguments);
            var offset = Vector3F.Zero;
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (entityUniverseFacade.TileOffset(tile, Location, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tileOffset, 1)) {
                    offset = tileOffset;
                }
            }
            Entity.Physics.MakePhysicsless();
            Entity.Physics.ForcedPosition(Location.ToTileCenterVector3D() + offset.ToVector3D());
            Entity.Physics.ValidateBoundingShape();
            Entity.Physics.CollisionShape.Center += offset.ToVector3D();
            Entity.Physics.BoundingShape.Center += offset.ToVector3D();
            _needsStore = true;
        }

        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }
        public override void BeingLookedAt(Entity entity) { }
        public override bool IsBeingLookedAt() {
            return false;
        }

        public override void Store() {
            if (_needsStore) {
                base.Store();
                Entity.Blob.SetLong("blockColor", BlockColor.PackedValue);
                Entity.Blob.SetString("tile", Configuration.Code);
                Entity.Blob.FetchBlob("location").SetVector3I(Location);

                _needsStore = false;
            }
        }

        public override void Restore() {
            if (Entity.Blob.Contains("blockColor")) {
                if (uint.TryParse(Entity.Blob.GetLong("blockColor").ToString(), out var packed)) {
                    BlockColor = ColorMath.FromRgba(packed);
                    UpdateColor = true;
                }
            }
            if (Entity.Blob.Contains("tile")) {
                Configuration = GameContext.TileDatabase.GetTileConfiguration(Entity.Blob.GetString("tile"));
                Configuration.ExportColor = Color.Transparent;
            }

            if (Entity.Blob.Contains("location")) {
                Location = Entity.Blob.GetBlob("location").GetVector3I();
            }
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);
            data.SetLong("blockColor", BlockColor.PackedValue);
            data.FetchBlob("constructor").MergeFrom(_constructArguments);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);
            if (data.Contains("blockColor")) {
                if (uint.TryParse(data.GetLong("blockColor").ToString(), out var packed)) {
                    BlockColor = ColorMath.FromRgba(packed);
                    UpdateColor = true;
                }
            }

            if (data.Contains("constructor")) {
                Construct(data.GetBlob("constructor"), facade);
            }
        }

        public void SetColor(Color color) {
            BlockColor = color;
            _needsStore = true;
        }

        public override bool IsCollidable() {
            return true;
        }
    }
}
