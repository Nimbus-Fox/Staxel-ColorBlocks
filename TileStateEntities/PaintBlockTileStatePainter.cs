﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ColorBlocks.Components;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Components;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;
using Constants = Staxel.Core.Constants;
using Helpers = NimbusFox.KitsuneCore.Helpers;

namespace NimbusFox.ColorBlocks.TileStateEntities {
    public class PaintBlockTileStatePainter : EntityPainter {

        private MultipleMatrixDrawable _tile;
        private uint _rotation = 0;
        private Vector3F _offset = new Vector3F(0.0f, 0.0f, 0.0f);

        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) {
            if (entity.Logic is PaintBlockTileStateLogic logic) {
                if (logic.UpdateColor || _tile == null) {
                    UpdateModel(logic.Configuration, logic.BlockColor);
                    logic.UpdateColor = false;
                }
                //if (facade.ReadTile(logic.Location, TileAccessFlags.None, out var tile)) {
                //    _rotation = tile.Configuration.Rotation(tile.Variant());
                //    UpdateOffset(tile, logic.Location, facade);
                //}
            }

            if (entity.Logic is PaintBucketTileStateEntityLogic bucketLogic) {
                if (bucketLogic.UpdateColor || _tile == null) {
                    UpdateModel(bucketLogic.Configuration, bucketLogic.BlockColor);
                    bucketLogic.UpdateColor = false;
                }
//                if (facade.ReadTile(bucketLogic.Location, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
//                    _rotation = tile.Configuration.Rotation(tile.Variant());
//                    UpdateOffset(bucketLogic.Location, facade);
//                }
            }
        }
        
        private static readonly Dictionary<Color, Color> Recolors = new Dictionary<Color, Color>();

        private void UpdateModel(TileConfiguration configuration, Color color) {
            var component = configuration.Components.Select<VoxelRecolorComponent>().FirstOrDefault();
            
            if (component != default(VoxelRecolorComponent)) {
                Recolors.Clear();
                foreach (var col in component.ColorCoords.Keys) {
                    if (col.R != col.G || col.G != col.B) {
                        Recolors.Add(col, col);
                        continue;
                    }
                    var shade = new Color(byte.MaxValue - col.R, byte.MaxValue - col.G, byte.MaxValue - col.B);
                    Recolors.Add(col, new Color(
                        Helpers.GetNewByte(color.R, shade.R),
                        Helpers.GetNewByte(color.G, shade.G),
                        Helpers.GetNewByte(color.B, shade.B)
                    ));
                }
                _tile = VoxelRecolor.FetchDrawable(configuration, Recolors);
            }
        }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) {
        }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController,
            Timestep renderTimestep) {
        }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (_tile != null) {
                if (entity.Logic is PaintBlockTileStateLogic paintBlock) {
                    var targetMatrix = matrix
                        .Translate(_tile.VoxelOffsets)
                        .Translate(-_tile.VoxelOffsets)
                        .Translate((paintBlock.Location.ToVector3F() + _offset) - renderOrigin.ToVector3F());
                    _tile.Render(graphics, ref targetMatrix);
                } else if (entity.Logic is PaintBucketTileStateEntityLogic paintBucket) {
                    var targetMatrix = matrix
                        .Translate(_tile.VoxelOffsets)
                        .Translate(-_tile.VoxelOffsets)
                        .Translate((paintBucket.Location.ToVector3F() + _offset) - renderOrigin.ToVector3F());
                    _tile.Render(graphics, ref targetMatrix);
                }
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
