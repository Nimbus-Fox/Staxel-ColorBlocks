﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ColorBlocks.TileStateEntities {
    public class PaintBlockTileStateBuilder : ITileStateBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorblocks.tilestate.paintBlock";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return PaintBlockTileStateEntityBuilder.Spawn(location, universe, tile);
        }
    }
}
