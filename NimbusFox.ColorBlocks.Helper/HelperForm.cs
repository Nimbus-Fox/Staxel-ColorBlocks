﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using NimbusFox.FoxCore.V3;
using NimbusFox.FoxCore.V3.Classes.FxBlob;
using Plukit.Base;
using Staxel.Voxel;

namespace NimbusFox.ColorBlocks.Helper {
    public partial class HelperForm : Form {

        private OpenFileDialog OpenFileDialog { get; }

        private Dictionary<Color, List<Vector3I>> _colors = new Dictionary<Color, List<Vector3I>>();

        public HelperForm() {
            InitializeComponent();

            OpenFileDialog = new OpenFileDialog {Multiselect = false, CheckPathExists = true};

            lstColors.View = View.List;
        }

        private void btnQbBrowse_Click(object sender, EventArgs e) {
            OpenFileDialog.CheckFileExists = true;
            OpenFileDialog.Filter = @"Qubicle binary (*.qb)|*.qb";
            if (OpenFileDialog.ShowDialog() == DialogResult.OK) {
                txtQbLocation.Text = OpenFileDialog.FileName;
                lblQbName.Text = OpenFileDialog.SafeFileName;
            }

            if (!string.IsNullOrEmpty(txtQbLocation.Text)) {
                btnScan.Enabled = true;
            }
        }

        private void BtnScan_Click(object sender, EventArgs e) {
            _colors.Clear();
            lstColors.Clear();
            using (var stream = new FileStream(txtQbLocation.Text, FileMode.Open)) {
                stream.Seek(0L, SeekOrigin.Begin);
                using (var mem = new MemoryStream()) {
                    stream.CopyTo(mem);
                    mem.Seek(0L, SeekOrigin.Begin);
                    var voxels = VoxelLoader.LoadQb(mem, txtQbLocation.Text, Vector3I.Zero, Vector3I.MaxValue);

                    Helpers.VectorLoop(Vector3I.Zero, voxels.Size, (x, y, z) => {
                        try {
                            var index = voxels.ColorData[x + z * voxels.Size.X + y * voxels.Size.X * voxels.Size.Z];

                            if (index.A == 0) {
                                return;
                            }

                            if (!_colors.ContainsKey(index)) {
                                _colors.Add(index, new List<Vector3I>());
                            }

                            _colors[index].Add(new Vector3I(x, y, z));
                        } catch {
                            // ignore
                        }
                    });
                }
            }

            var minContrast = 0.5f;

            foreach (var color in _colors) {
                var item = new ListViewItem {
                    Text = $@"R: {color.Key.R} G: {color.Key.G} B: {color.Key.B} ({color.Value.Count} voxels)",
                    BackColor = System.Drawing.Color.FromArgb(color.Key.R, color.Key.G, color.Key.B)
                };

                if (!IsContrastReadable(item.BackColor, item.ForeColor)) {
                    item.ForeColor = System.Drawing.Color.White;
                }

                lstColors.Items.Add(item);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            var blob = new FoxBlob();

            var keys = _colors.Keys.ToList();

            for (var i = 0; i < lstColors.Items.Count; i++) {
                var item = lstColors.Items[i];

                if (item.Checked) {
                    var color = keys[i];

                    blob.SetList(color.PackedValue.ToString(), _colors[color]);
                }
            }

            var saveDialog = new SaveFileDialog {CreatePrompt = true, OverwritePrompt = true};
            if (saveDialog.ShowDialog() == DialogResult.OK) {
                File.WriteAllText(saveDialog.FileName, blob.ToString());
            }
        }

        private static bool IsContrastReadable(System.Drawing.Color color1, System.Drawing.Color color2) {
            const float minContrast = 0.5f;

            var brightness1 = color1.GetBrightness();
            var brightness2 = color2.GetBrightness();
            
            return Math.Abs(brightness1 - brightness2) >= minContrast;
        }
    }
}
