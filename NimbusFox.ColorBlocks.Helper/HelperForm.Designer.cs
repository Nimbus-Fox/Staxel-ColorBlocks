﻿namespace NimbusFox.ColorBlocks.Helper {
    partial class HelperForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtQbLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQbBrowse = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.lblQbName = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.lstColors = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // txtQbLocation
            // 
            this.txtQbLocation.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtQbLocation.Enabled = false;
            this.txtQbLocation.Location = new System.Drawing.Point(45, 10);
            this.txtQbLocation.Name = "txtQbLocation";
            this.txtQbLocation.Size = new System.Drawing.Size(208, 20);
            this.txtQbLocation.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Qb:";
            // 
            // btnQbBrowse
            // 
            this.btnQbBrowse.Location = new System.Drawing.Point(259, 8);
            this.btnQbBrowse.Name = "btnQbBrowse";
            this.btnQbBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnQbBrowse.TabIndex = 2;
            this.btnQbBrowse.Text = "Browse";
            this.btnQbBrowse.UseVisualStyleBackColor = true;
            this.btnQbBrowse.Click += new System.EventHandler(this.btnQbBrowse_Click);
            // 
            // btnScan
            // 
            this.btnScan.Enabled = false;
            this.btnScan.Location = new System.Drawing.Point(12, 53);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(316, 23);
            this.btnScan.TabIndex = 6;
            this.btnScan.Text = "Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.BtnScan_Click);
            // 
            // lblQbName
            // 
            this.lblQbName.Location = new System.Drawing.Point(16, 35);
            this.lblQbName.Name = "lblQbName";
            this.lblQbName.Size = new System.Drawing.Size(312, 18);
            this.lblQbName.TabIndex = 7;
            this.lblQbName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 184);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(316, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lstColors
            // 
            this.lstColors.CheckBoxes = true;
            this.lstColors.Location = new System.Drawing.Point(12, 83);
            this.lstColors.MultiSelect = false;
            this.lstColors.Name = "lstColors";
            this.lstColors.Size = new System.Drawing.Size(316, 95);
            this.lstColors.TabIndex = 11;
            this.lstColors.UseCompatibleStateImageBehavior = false;
            // 
            // HelperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 215);
            this.Controls.Add(this.lstColors);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblQbName);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnQbBrowse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtQbLocation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "HelperForm";
            this.Text = "Color Blocks Helper";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtQbLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQbBrowse;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label lblQbName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListView lstColors;
    }
}

