﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.ColorBlocks.Components.Tile {
    public class PaintBlockComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "paintBlock";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new PaintBlockComponent(config);
        }
    }
}
