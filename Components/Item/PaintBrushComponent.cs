﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ColorBlocks.Components.Item {
    public class PaintBrushComponent {
        public Vector2I Size = Vector2I.Zero;

        public PaintBrushComponent(Blob config) {
            if (config.Contains("size")) {
                Size = config.FetchBlob("size").GetVector2I();
            }
        }

        public PaintBrushComponent() {

        }
    }
}
