﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.ColorBlocks.Components.Item {
    public class PaintBrushComponentBuilder : IItemComponentBuilder {
        public string Kind() {
            return "paintBrush";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new PaintBrushComponent(config);
        }
    }
}
