﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Tiles.TileComponents;
using Staxel.Voxel;

namespace NimbusFox.ColorBlocks.Components {
    public class PaintBlockComponent : ITileComponent {
        public string Tile { get; }
        public IReadOnlyDictionary<Color, List<Vector3I>> ColorCoords = new Dictionary<Color, List<Vector3I>>();
        public VoxelObject Voxels;

        public PaintBlockComponent(Blob config) {
            if (config.Contains("tile")) {
                Tile = config.GetString("tile");
            }

            if (config.Contains("colorConfig")) {
                using (var stream = GameContext.ContentLoader.ReadStream(config.GetString("colorConfig"))) {
                    stream.Seek(0L, SeekOrigin.Begin);

                    var blob = BlobAllocator.Blob(true);
                    blob.LoadJsonStream(stream);

                    var data = new Dictionary<Color, List<Vector3I>>();

                    foreach (var entry in blob.KeyValueIteratable) {
                        if (uint.TryParse(entry.Key, out var key)) {
                            var color = ColorMath.FromRgba(key);

                            var list = new List<Vector3I>();

                            var blobList = entry.Value.List();

                            foreach (var bl in blobList) {
                                var blb = bl.Blob();
                                list.Add(new Vector3I((int)blb.GetLong("X"), (int)blb.GetLong("Y"), (int)blb.GetLong("Z")));
                            }

                            data.Add(color, list);
                        }
                    }

                    ColorCoords = data;
                }
            }

            if (config.Contains("qbSource")) {
                Voxels = VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(config.GetString("qbSource")),
                    config.GetString("qbSource"), Vector3I.Zero, Vector3I.MaxValue);
            }
        }

        public void Check() { }
    }
}
